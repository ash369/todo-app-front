import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Todo } from 'src/app/models/update-todo.model';
import { ApiConnectorService } from 'src/app/services/api-connector.service';

@Component({
  selector: 'app-todo-items-edit',
  templateUrl: './todo-items-edit.component.html',
  styleUrls: ['./todo-items-edit.component.css']
})
export class TodoItemsEditComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<TodoItemsEditComponent>,
              @Inject(MAT_DIALOG_DATA) public todo: Todo,
              private apiConnectorService: ApiConnectorService) { }

  todoToUpdate: Todo;
  todoBackup: Todo;

  ngOnInit(): void {
    this.todoToUpdate = JSON.parse(JSON.stringify(this.todo));
    this.todoBackup = JSON.parse(JSON.stringify(this.todo));
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onSave(): void {
    if(this.todoToUpdate.title.trim() === "") {
      var deleteTodo = this.todoToUpdate.id;
      this.dialogRef.close(deleteTodo);
    }
    else {
      this.todo.title = this.todoToUpdate.title;
      this.apiConnectorService.updateTodo(this.todo).subscribe((result) => {
      }, error => {
        this.todo.title = this.todoBackup.title;
      }, () => {
        this.dialogRef.close();
      });
    }
  }


}
