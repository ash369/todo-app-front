import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-todo-items-active',
  templateUrl: './todo-items-active.component.html',
  styleUrls: ['./todo-items-active.component.css']
})
export class TodoItemsActiveComponent implements OnInit {
  todoType = "Active"
  constructor() { }

  ngOnInit(): void {
  }

}
