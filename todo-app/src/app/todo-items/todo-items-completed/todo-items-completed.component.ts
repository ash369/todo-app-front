import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-todo-items-completed',
  templateUrl: './todo-items-completed.component.html',
  styleUrls: ['./todo-items-completed.component.css']
})
export class TodoItemsCompletedComponent implements OnInit {
  todoType = "Completed"
  constructor() { }

  ngOnInit(): void {
  }

}
