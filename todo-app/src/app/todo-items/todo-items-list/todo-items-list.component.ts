import { Component, OnInit, Input } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import {SelectionModel} from '@angular/cdk/collections';

import { ApiConnectorService } from 'src/app/services/api-connector.service';
import { Todo } from '../../models/update-todo.model';
import { TodoItemsEditComponent } from '../todo-items-edit/todo-items-edit.component';
import { AddTodo } from 'src/app/models/add-todo.model';

@Component({
  selector: 'app-todo-items-list',
  templateUrl: './todo-items-list.component.html',
  styleUrls: ['./todo-items-list.component.css']
})
export class TodoItemsListComponent implements OnInit {
  @Input() todoType: string;
  todoAdd: AddTodo = {title: '', active: true};
  todoDataLength: number;
  todoData: any = [];
  todosAll: any = [];
  todoAllNonFiltered: any = [];
  todosToDelete: any = [];
  tableHeading: string = 'My Todos';
  dataSource;
  selection;
  isMarkAllTodoChecked: boolean = false;
  numberOfActiveTodos;
  numberOfCompletedTodos;
  numberTodos;
  loading: boolean = true;
  inputError = false;
  timeLeft: number = 3;
  interval;

  constructor(private apiConnectorService: ApiConnectorService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.apiConnectorService.getTodosAll().subscribe((data) => {
      this.todoAllNonFiltered = data;
      if(this.todoType == 'All'){
        this.todosAll = data;
      }
      else if(this.todoType == 'Active'){
        this.todoData = JSON.parse(JSON.stringify(data));
        this.todosAll = this.todoData.filter(item => item.active == 1);
        this.tableHeading = 'My Active Todos';
      }
      else if(this.todoType == 'Completed') {
        this.todoData = JSON.parse(JSON.stringify(data));
        this.todosAll = this.todoData.filter(item => item.active == 0);
        this.tableHeading = 'My Completed Todos';
      }

    }, error => {
    },
    () => {
      this.numberTodos = this.todosAll.length;
      this.dataSource = new MatTableDataSource<Todo>(this.todosAll);
      this.selection = new SelectionModel<Todo>(true, []);
      this.loading = false;
    });

    this.apiConnectorService.getNumberActiveTodos().subscribe((data) => {
      this.numberOfActiveTodos = data;
    }, error => {
    });

    this.apiConnectorService.getNumberCompletedTodos().subscribe((data) => {
      this.numberOfCompletedTodos = data;
    }, error => {
    },
    () => {
      this.todoDataLength = this.todoAllNonFiltered.length;
      if(this.numberOfCompletedTodos == this.todoDataLength){
        this.isMarkAllTodoChecked = true
      }
    });
  }

  displayedColumns: string[] = ['select', 'title', 'delete'];

  addTodo($event) {
    if(this.todoAdd.title.trim() == '' || this.todoAdd.title == null) {
      this.inputError = true;
      this.startTimer();
    } else {
      this.inputError = false;
      this.apiConnectorService.addTodo(this.todoAdd).subscribe((data) => {
        this.todosAll.push(data);
        if(this.todoType !== 'Completed'){
          this.dataSource._updateChangeSubscription();
        }
        this.todoAdd = {title: '', active: true};
        this.numberOfActiveTodos++;
      }, error => {
      }, () => {
        this.numberTodos = this.todosAll.length;
      });
    }
  }

  startTimer() {
    this.timeLeft = 3;
    this.interval = setInterval(() => {
      if(this.timeLeft > 0) {
        this.timeLeft--;
      } else {
        this.timeLeft = 3;
        this.inputError = false;
      }
    },1000)
  }

  markTodo(event, todoToUpdate) {
    todoToUpdate.active = !event.checked;

    if(event.checked){
      this.numberOfCompletedTodos++;
      this.numberOfActiveTodos--;
    } else {
      this.numberOfCompletedTodos--;
      this.numberOfActiveTodos++;
    }

    this.apiConnectorService.updateTodo(todoToUpdate).subscribe((result) => {
    }, error => {
      todoToUpdate.active = !event.checked;
      if(event.checked){
        this.numberOfCompletedTodos--;
        this.numberOfActiveTodos++;
      } else {
        this.numberOfCompletedTodos++;
        this.numberOfActiveTodos--;
      }
    });
  }

  markAllTodos(event) {
    var todosToUpdate = [];
    var numberTodosMarked;
    this.isMarkAllTodoChecked = event.checked;

    if(this.isMarkAllTodoChecked) {
      for(let i = 0; i < this.todosAll.length; i++) {
        if(this.todosAll[i].active == true) {
          this.todosAll[i].active = false;
          todosToUpdate.push(this.todosAll[i]);
        }
      }
      numberTodosMarked = todosToUpdate.length;
      this.numberOfActiveTodos = this.numberOfActiveTodos - numberTodosMarked;
      this.numberOfCompletedTodos = this.numberOfCompletedTodos + numberTodosMarked;
    }
    else {
      for(let i = 0; i < this.todosAll.length; i++) {
        if(this.todosAll[i].active == false) {
          this.todosAll[i].active = true;
          todosToUpdate.push(this.todosAll[i]);
        }
      }
      numberTodosMarked = todosToUpdate.length;
      this.numberOfActiveTodos = this.numberOfActiveTodos + numberTodosMarked;
      this.numberOfCompletedTodos = this.numberOfCompletedTodos - numberTodosMarked;
    }

    this.apiConnectorService.updateTodos(todosToUpdate).subscribe((result) => {
    }, error => {
      for(let i = 0; i < this.todosAll.length; i++) {
        if(this.isMarkAllTodoChecked) {
          this.todosAll[i].active = true;
        }
        else {
          this.todosAll[i].active = false;
        }

      }
    });
  }

  deleteTodo(todoId) {
    var todoToDelete = this.todosAll.find(function(todo){
      return todo.id === todoId;
    });

    var indexPositionOfTodo = this.todosAll.findIndex(function(todo){
      return todo.id === todoId;
    });

    this.todosAll.splice(indexPositionOfTodo, 1);
    this.dataSource._updateChangeSubscription();
    this.numberOfActiveTodos--;

    this.apiConnectorService.deleteTodo(todoId).subscribe((result) => {
    }, error => {
      this.todosAll.push(todoToDelete);
      this.dataSource._updateChangeSubscription();
      this.numberOfActiveTodos++;
    }, () => {
      this.numberTodos = this.todosAll.length;
    });
  }

  deleteTodos() {
    var todosToDeleteArrayPosition = [];

    for(let i = 0; i < this.todosAll.length; i++) {
      if(this.todosAll[i].active == false) {
        todosToDeleteArrayPosition.push(i);
      }
    }

    for(let i = 0; i < todosToDeleteArrayPosition.length; i++) {
      this.apiConnectorService.deleteTodo(this.todosAll[todosToDeleteArrayPosition[i]].id).subscribe((result) => {
      }, error => {
      });
    }

    for (var i = todosToDeleteArrayPosition.length -1; i >= 0; i--) {
      this.todosAll.splice(todosToDeleteArrayPosition[i],1);
    }
    this.dataSource._updateChangeSubscription();

    this.numberOfCompletedTodos = this.numberOfCompletedTodos - todosToDeleteArrayPosition.length;
    this.numberTodos = this.todosAll.length;
  }

  openDialog(todo): void {
    const dialogRef = this.dialog.open(TodoItemsEditComponent, {
      width: '250px',
      data: todo,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.deleteTodo(result);
      }
    });

    dialogRef.backdropClick().subscribe(() => {
      dialogRef.close();
    })
  }

}
