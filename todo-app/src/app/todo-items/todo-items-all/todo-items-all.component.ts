import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-todo-items-all',
  templateUrl: './todo-items-all.component.html',
  styleUrls: ['./todo-items-all.component.css']
})
export class TodoItemsAllComponent implements OnInit {
  todoType = "All"
  constructor() { }

  ngOnInit(): void {
  }

}
