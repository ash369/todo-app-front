import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TodoItemsAllComponent } from './todo-items/todo-items-all/todo-items-all.component';
import { TodoItemsActiveComponent } from './todo-items/todo-items-active/todo-items-active.component';
import { TodoItemsCompletedComponent } from './todo-items/todo-items-completed/todo-items-completed.component';

const routes: Routes = [
  { path:  '', redirectTo: 'all', pathMatch:  'full' },
  {
    path: 'all',
    component: TodoItemsAllComponent
  },
  {
    path: 'active',
    component: TodoItemsActiveComponent
  },
  {
    path: 'completed',
    component: TodoItemsCompletedComponent
  },
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
