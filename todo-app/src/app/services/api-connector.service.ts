import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { AddTodo } from '../models/add-todo.model';
import { Todo } from '../models/update-todo.model';

@Injectable({
  providedIn: 'root'
})

export class ApiConnectorService {
  baseUrl = 'https://todo-app-ash-api.azurewebsites.net/api/todos';

  constructor(private http: HttpClient) { }

  addTodo(todo: AddTodo) {
    return this.http.post(this.baseUrl, todo);
  }

  getNumberActiveTodos(){
    return this.http.get(this.baseUrl + '/number-active-todos');
  }

  getNumberCompletedTodos(){
    return this.http.get(this.baseUrl + '/number-completed-todos');
  }

  getTodosAll(){
    return this.http.get(this.baseUrl);
  }

  updateTodo(todo: Todo){
    return this.http.put(this.baseUrl + '/' + todo.id, todo);
  }

  updateTodos(todos){
    return this.http.put(this.baseUrl, todos);
  }

  deleteTodo(todoId) {
    return this.http.delete(this.baseUrl + '/' + todoId);
  }

  deleteTodos(todos) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      }),
      data: JSON.stringify(todos)
    };


    return this.http.delete(this.baseUrl, httpOptions);
  }

}
