import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';

import { ApiConnectorService } from './services/api-connector.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarTopComponent } from './navbar-top/navbar-top.component';
import { NavbarFooterComponent } from './navbar-footer/navbar-footer.component';

import { TodoItemsAllComponent } from './todo-items/todo-items-all/todo-items-all.component';
import { TodoItemsActiveComponent } from './todo-items/todo-items-active/todo-items-active.component';
import { TodoItemsCompletedComponent } from './todo-items/todo-items-completed/todo-items-completed.component';
import { TodoItemsListComponent } from './todo-items/todo-items-list/todo-items-list.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TodoItemsEditComponent } from './todo-items/todo-items-edit/todo-items-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarTopComponent,
    NavbarFooterComponent,
    TodoItemsAllComponent,
    TodoItemsActiveComponent,
    TodoItemsCompletedComponent,
    TodoItemsListComponent,
    TodoItemsEditComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    AppRoutingModule,
    MatCheckboxModule,
    MatDialogModule,
    MatButtonModule,
    MatToolbarModule,
    MatSidenavModule
  ],
  providers: [ApiConnectorService],
  bootstrap: [AppComponent]
})
export class AppModule { }
