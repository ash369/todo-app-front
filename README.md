# Todo Application Backend

This was built as part of a Developer Bootcamp Project.  

This is the frontend web application for todos app and works with the todo-app-back project.
Part of the project was to run the application in a cloud provider of our choice inside docker containers, as well as have pipelines that tests, builds and pushes the images to the cloud provider.

A full list of requirements for the project can be found here: [awesome-bootcamp-group/todo-base](https://gitlab.com/awesome-bootcamp-group/todo-base)

## Application Details
The application was built using Angular 9 together with the following:
- Angular Material with a custom dark theme
- FontAwesome