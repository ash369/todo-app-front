import { Component, OnInit, Input } from '@angular/core';
import { ApiConnectorService } from '../services/api-connector.service';
import { AddTodo } from '../models/add-todo.model';

@Component({
  selector: 'app-todo-add',
  templateUrl: './todo-add.component.html',
  styleUrls: ['./todo-add.component.css']
})
export class TodoAddComponent implements OnInit {
  todo: AddTodo = {title: '', active: true};
  numberActiveTodos = undefined;

  constructor(private apiConnectorService: ApiConnectorService) { }

  ngOnInit(): void {
    this.getNumberActiveTodos();
  }

  addTodo($event) {
    this.apiConnectorService.addTodo(this.todo).subscribe(() => {
      this.todo = {title: '', active: true};
      this.numberActiveTodos++;
    }, error => {
    });
  }

  getNumberActiveTodos() {
    this.apiConnectorService.getNumberActiveTodos().subscribe((result) => {
      this.numberActiveTodos = result;
    }, error => {
    });
  }

}
