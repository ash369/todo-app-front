FROM node:12.7-alpine AS build
WORKDIR /usr/src/app
COPY todo-app/ .
RUN npm install -g
RUN npm run build

FROM nginx:1.15.8-alpine
COPY --from=build /usr/src/app/dist/todo-app/ /usr/share/nginx/html